
<%@page import="Model.Static"%>
<%@page import="Model.ItemQuantidade"%>
<%@page import="org.apache.catalina.Session"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Model.Item"%>
<%@page import="java.awt.event.ItemEvent"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="Model.Carrinho"%>
<%@page import="Model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%!
    
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Carrinho de Compras</title>
        
        <script lang="javascript">
            function ajustaInput(id){
                document.getElementById("removerItem").value = id;
                return true;
            }
        
    
</script>
        
        <style>
            td.um{
                border: solid black 2px;
                background-color: aquamarine;
            }
            td.dois{
                border: solid black 2px;
                background-color: gainsboro;
            }
            input.quant{
                width: 60px;
            }
            #InfoCarrinho{
               float: left;
               
            }
            #ItensCarrinho{
                /*position: static;*/
                border: solid black 1px;
                float: right;
                width: 300px;
                height: 150px;   
                background-color: snow;
            }
        </style>

    </head>
    <body>
        <%
            Usuario user = (Usuario) session.getAttribute("usuario");
            Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
            //out.print(carrinho);
            ArrayList<Item> lista = (ArrayList<Item>) session.getAttribute("listaItens");
        %>
        <div style="height: 100px">
                Você está logado como: <strong>
                    <% out.print(user.getNome());%>
                </strong>
                Seu Email:
                <strong>
                    <%
                        out.print(user.getEmail());
                    %>  
                </strong>
                <br/>
                <br/>
            <div id="InfoCarrinho">
                Você possui <strong><% out.print(carrinho.getTotalItens());%></strong> itens em seu carrinho
                <br/>
                Valor Total: <strong><% out.print(Static.convertToCurrency(carrinho.getValorTotal()));%></strong>
            </div>
            <div id="ItensCarrinho">
                <strong>Produtos no seu carrinho:</strong>
                <br/>
                <form action="removerItem" method="post">
                    <input type="hidden" id="removerItem" name="removerItem" value="88">  
                <table>
                    <th>Item</th>
                    <th>Quant.</th>
<% 
    for(ItemQuantidade iq:carrinho.getItens().values()){
%>
<tr>
    <td>
        <%=iq.getItem().getNome()%>
    </td>
    <td>
        <%=iq.getQuantidade()%>
    </td>
    <td>
        <input type="submit" value="Remover" onclick="ajustaInput(<%=iq.getItem().getId()%>)">
    </td>   
</tr>
                    
                    
<%
}
%>
                </table>
                 
            </form>
               
            </div>
        </div>
        <h1>Escolha seu produto</h1>
        <Form action="adicionarItem" method="post">
            <table>
                <thead>
                    <tr>
                        <th>Produto</th>
                        <th>Descrição</th>
                        <th>Preço</th>
                        <th>Quantidade</th>

                    </tr>
                </thead>
                <tbody>
                    <%
                        for (Item item : lista) {
                    %>
                    <tr>
                        <td class="um"><%=item.getNome()%></td>
                        <td class="dois"><%=item.getDescricao()%></td>
                        <td class="um"><%=Static.convertToCurrency(item.getPreco())%></td>
                        <td class="dois"><input type="text" name="quantidade<%=item.getId()%>" value="0" class="quant"></td>    
                    </tr>           
                    <%
                        }
                    %>

                </tbody>
            </table>    
            <input type="submit" value="Adicionar" />
            <br/><br/>

        </Form>
        <form action="compra1.jsp" method="Post">
            <input type="submit" value="Finalizar Compra">
        </form>    

    </body>
</html>
