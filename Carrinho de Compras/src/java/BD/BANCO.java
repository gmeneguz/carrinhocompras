/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package BD;

import Model.Item;
import java.util.ArrayList;

/**
 *
 * @author desenvolvedor01
 */
public class BANCO {
    private ArrayList<Item> itens;
    
    public BANCO(){
        itens = new ArrayList<Item>();
        
        Item item = new Item(1, "Tenis Preto Adidas","Tênis de camursa Preto modelo A55 esporte",278.00,0.2);
        itens.add(item);
        item = new Item(2, "Tenis Branco Adidas","Tênis de camursa Branco modelo A40 esporte",154.00,0.3);
        itens.add(item);
        item = new Item(3, "Tenis corrida Asics","Tênis para atletas de final de semana modelo leve",300.00,0.2);
        itens.add(item);
    }
    public ArrayList<Item> getItens(){
        return itens;
    }
    public Item selectId(int id){
        if(id > 0 && id <= itens.size()){
            return itens.get(id-1);
        }
        return null;
    }
}
