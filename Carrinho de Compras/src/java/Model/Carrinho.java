/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import BD.BANCO;
import java.security.KeyPair;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author desenvolvedor01
 */
public class Carrinho {
    private HashMap<Integer,ItemQuantidade> itens;
    private double valorTotal;
    
    public Carrinho(){
        itens = new HashMap<Integer,ItemQuantidade>();
        valorTotal = 0;
    }

    public HashMap<Integer,ItemQuantidade> getItens() {
        return itens;
    }

    public void setItens(HashMap<Integer,ItemQuantidade> itens) {
        this.itens = itens;
    }

    public double getValorTotal() {
        double total = 0;
        for (int key : itens.keySet()) {
           total += itens.get(key).getItem().getPreco() * itens.get(key).getQuantidade();
        }
        valorTotal = total;
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }
    public int getTotalItens(){
        int total = 0;
        for (int key : itens.keySet()) {
           total += itens.get(key).getQuantidade();
        }
        return total;
    }
    public void addItem(int id, int quantidade){
        BANCO bd = new BANCO();
        Item item = bd.selectId(id);
        ItemQuantidade iq = new ItemQuantidade();
        iq.setItem(item);
        iq.setQuantidade(quantidade);
        this.itens.put(id, iq);
    }
    public void removeItem(int id){
        if(itens.containsKey(id)){
            itens.remove(id);
        }
    }
    public void limparCarrinho(){
        for(int key : itens.keySet() ){
            itens.remove(key);
        }
    }
}
