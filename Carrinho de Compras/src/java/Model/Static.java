/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DecimalFormat;

/**
 *
 * @author desenvolvedor01
 */
public class Static {
    public static int tryParseInt(String value)  
    {  
        try  
        {  
            int val;
             val = Integer.parseInt(value);  
             return val;  
        }catch(NumberFormatException nfe)  
        {  
            return 0;  
        }  
    }
    public static String convertToCurrency(double number) {
        DecimalFormat formato = new DecimalFormat("R$" + "#,##0.00");
        return formato.format(number);
    }

}
