/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import Model.Carrinho;
import Model.ItemQuantidade;
import Model.Static;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.catalina.Session;

/**
 *
 * @author desenvolvedor01
 */
public class adicionarItem extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet adicionarItem</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet adicionarItem at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher;
        HttpSession sessaoAdm = request.getSession(); 
        if (sessaoAdm.isNew()) {
            //Session has expired - redirect to login.jsp
            response.sendRedirect("index.jsp");
        }
         
        //processRequest(request, response);
        
        
        int quant1 = Static.tryParseInt(request.getParameter("quantidade1"));
        int quant2 = Static.tryParseInt(request.getParameter("quantidade2"));
        int quant3 = Static.tryParseInt(request.getParameter("quantidade3"));
        
        HttpSession session = request.getSession();
    
        Carrinho carrinho = (Carrinho)session.getAttribute("carrinho");
        
        if(carrinho == null){
            return;
        }
        
        if(quant1 > 0){
            if(carrinho != null && carrinho.getItens().get(1) != null){
                carrinho.getItens().get(1).setQuantidade(carrinho.getItens().get(1).getQuantidade() + quant1);
            }
            else{
               carrinho.addItem(1, quant1);
            }
        }
        if(quant2 > 0){
           if(carrinho.getItens().get(2) != null){
                carrinho.getItens().get(2).setQuantidade(carrinho.getItens().get(2).getQuantidade() + quant2);
            }
            else{
                carrinho.addItem(2, quant2);
            }
        }
        if(quant3 > 0){
           if(carrinho.getItens().get(3) != null){
                carrinho.getItens().get(3).setQuantidade(carrinho.getItens().get(3).getQuantidade() + quant3);
            }
            else{
                carrinho.addItem(3, quant3);
            }
        }
        
        dispatcher = request.getRequestDispatcher("menu.jsp");
            dispatcher.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
