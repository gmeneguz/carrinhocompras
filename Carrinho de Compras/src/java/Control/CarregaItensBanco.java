/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Control;

import BD.BANCO;
import Model.Item;
import java.util.ArrayList;

/**
 *
 * @author desenvolvedor01
 */
public class CarregaItensBanco {
    
    public ArrayList<Item> carregarItens(){
        
        //* Arqui chamariamos a classe dao, mas no caso vamos chamar o classe BANCO com o sintens já formatados=)
        BANCO bd = new BANCO();
        return bd.getItens();
    }
}
