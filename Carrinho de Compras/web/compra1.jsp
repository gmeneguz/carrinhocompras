<%-- 
    Document   : compra1
    Created on : Apr 8, 2014, 1:43:47 PM
    Author     : desenvolvedor01
--%>

<%@page import="Model.Static"%>
<%@page import="Model.ItemQuantidade"%>
<%@page import="Model.Usuario"%>
<%@page import="Model.Carrinho"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    
        <%
            Usuario user = (Usuario) session.getAttribute("usuario");
            Carrinho carrinho = (Carrinho) session.getAttribute("carrinho");
            int valorFrete =  Static.tryParseInt((String)session.getAttribute("valorFrete"));
            
            String cidadeFrete =  (String)session.getAttribute("cidadeFrete");
            
            //out.print(carrinho);
            //ArrayList<Item> lista = (ArrayList<Item>) session.getAttribute("listaItens");
        %>
    
        <style>
            td{
                border: 1px solid black;
            }
        </style>
        
    </head>
    <body>
       <div style="height: 100px">
                Você está logado como: <strong>
                    <% out.print(user.getNome());%>
                </strong>
                Seu Email:
                <strong>
                    <%
                        out.print(user.getEmail());
                    %>  
                </strong>
       </div>
                
                <h1>     Caro cliente, confira e confirme a sua compra:</h1>
                <table>
                    <th>Item</th>
                    <th>Quant.</th>
                    <th>Preço</th>
                    <th>Total Produto</th>
<% 
    for(ItemQuantidade iq:carrinho.getItens().values()){
%>
<tr>
    <td>
        <%=iq.getItem().getNome()%>
    </td>
    <td>
        <%=iq.getQuantidade()%>
    </td>
    <td>
        <%=Static.convertToCurrency(iq.getItem().getPreco())%>
    </td>
    <td>
        <%=Static.convertToCurrency(iq.getItem().getPreco() * iq.getQuantidade())%>
    </td>
</tr>
        <%
                        }
                    %>  
                    <tr>
                        <td>Frete</td><td></td><td></td><td> <%=Static.convertToCurrency(valorFrete)%></td>
                            
                    </tr>
<tr>
    <td><Strong>
            TOTAL
        </Strong>
    </td>
    <td>
    </td>
    <td>
    </td>
    <td><strong><%=Static.convertToCurrency(carrinho.getValorTotal()+valorFrete)%> </strong></td>
    
</tr>
</table>
    <br/><br/>
    <form action="CalculaFrete" method="Post">
    Calcule o frete, digite seu cep:
    <input type="text" name="cep">
    <input type="submit" value="Calcular">
    
    </form>
    <%
    if(cidadeFrete != null && !cidadeFrete.equals(""))
{
%>    
    Valor do frete para <strong id="endereco"><%=cidadeFrete%></strong> : <strong><%=Static.convertToCurrency(valorFrete)%></strong>
    <br/><br/>
    <form>
        <input type="submit" value="Confirmar">
        
    </form>
    <%
}
    else{
    %>
    <br/><br/>
    <form>
        <input type="submit" value="Confirmar" disabled="disabled">
        
    </form>
    <% 
    }
        %>
   </body>

</html>
