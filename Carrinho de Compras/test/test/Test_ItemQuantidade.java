/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import Model.Item;
import Model.ItemQuantidade;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author 0450103
 */
public class Test_ItemQuantidade {
    Item item;
    ItemQuantidade iq;

    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {        
    }
    /**
     * Teste para ItemQuantidade 
     */
    @Test
    public void test_ItemQUantidade(){
        item = new Item(1, "item1", "desc1", 50.0, 1.0);
        iq = new ItemQuantidade();
        iq.setItem(item);
        iq.setQuantidade(5);
        assertFalse(iq.getItem() == null);
        assertTrue(iq.getQuantidade() > 0);
    }
            
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
