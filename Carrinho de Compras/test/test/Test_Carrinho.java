/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import Model.Carrinho;
import static org.hamcrest.CoreMatchers.not;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author 0450103
 */
public class Test_Carrinho {
    Carrinho carrinho;
    
    public Test_Carrinho() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        carrinho = new Carrinho();
    }
    /**
     * Teste para adicionar item ao carrinho e verificar efetividade da adição.
     * Teste 1: Adicionamos o item e tentamos acessar o HashMap na posição 0.
     * Teste 2: Colocando 3 itens no carrinho e testando se possui 3 itens.
     */
    @Test
    public void test_AddItem(){
        carrinho.addItem(1, 5);
        assertNotNull("Carrinho deve retornar um item", carrinho.getItens().get(0));
        carrinho.addItem(2, 5);
        carrinho.addItem(3, 5);
        assertEquals("Carrinho deveria ter 3 itens", carrinho.getItens().size(), 3);
    }
    
    /**
     * Teste para para limpar, verificar valores e remover itens.
     * Teste 1: Limpar : O carrinho deve estar vazio depois da execução do método
     * Teste 2: Valores: O valor total dos itens do carrinho deve ser diferente de 0.
     * Teste 3: Remover: Os itens são removidos individualmente e após isto, o carrinho deve ter 0 itens
     */
    @Test
    public void test_AdicionarERemoverItem(){
        carrinho.limparCarrinho();
        assertEquals("Carrinho deveria estar vazio após remoção", carrinho.getItens().size(), 0);
        for(int i = 0;i < 4 ; i++){
            carrinho.addItem(i, 5);
        }
        assertThat("Valor total do carrinho deveria ser maior que zero", carrinho.getValorTotal(), not(0.0));
         for(int i = 0;i < 4 ; i++){
            carrinho.removeItem(i);
        }
        assertEquals("Carrinho deveria estar vazio após remoção", carrinho.getItens().size(), 0);
    }
    

    @After
    public void tearDown() {
        carrinho = null;
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
