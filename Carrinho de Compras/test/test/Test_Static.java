/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import Model.Static;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 0450103
 */
public class Test_Static {
    
    public Test_Static() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    
    /**
     * Teste de método "tryParseInt" para conversão de string para inteiro.
     * Difere do Integer.ParseInt() porque ao invés de lançar exceção, retorna 0
     * Dois testes realizados dentro de for
     * Teste 1: efetividade: o método deve retornar um valor positivo que corresponde a string
     * Teste 2: exceção: o método deve retornar 0 pois possui uma letra.
     */
    @Test
    public void test_Parse(){
        String numero1;
        String numero2;
        String numeroString;
        for(int i = 1; i < 10000 ; i++ )
        {
            for(int j = 999; j > 0 ; j ++){
                numero1 = String.valueOf(i);
                numero2 = String.valueOf(j);
                numeroString = numero1 + numero2;
                assertTrue("Deveria retornar maior que zero",Static.tryParseInt(numeroString) > 0);
            }
        }
        for(int i = 1; i < 10000 ; i++ )
        {
            for(int j = 999; j > 0 ; j ++){
                numero1 = String.valueOf(i);
                numero2 = String.valueOf(j);
                numeroString = "a" + numero1 + numero2;
                assertFalse("Deveria retornar zero",Static.tryParseInt(numeroString) > 0);
            }
        }
        
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
